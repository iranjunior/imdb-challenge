import { getGenres } from "../Services/api";
import { LOAD_GENRES } from "../Constants/actionsType";

export const genres = dispatch => {
  getGenres().then(response =>
    dispatch({ type: LOAD_GENRES, playload: response })
  );
};
