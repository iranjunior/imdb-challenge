import React, { useEffect } from "react";
import api from "../../Services/api";
import { api_key } from "../../Config/vars";
import {
  Container,
  Card,
  Title,
  CardContent,
  Content,
  ImageMovie,
  ImageField,
  Rated,
  ChipsField,
  Chip,
  Arc,
  Description
} from "./styles";
import { connect } from "react-redux";

import {
  LOAD_MOVIES,
  LOAD_GENRES,
  PAGES_TOTALS
} from "../../Constants/actionsType";

let filteredMovies = [];

const discover = async (dispatch, movies) => {
  if (movies.length < 1)
    await api
      .get(
        `https://api.themoviedb.org/3/discover/movie?api_key=${api_key}&language=pt-BR&sort_by=popularity.desc&include_adult=false&include_video=false&page=1`
      )
      .then(response => {
        dispatch({
          type: LOAD_MOVIES,
          payload: response.data.results
        });
        dispatch({
          type: PAGES_TOTALS,
          payload: response.data.total_results / 5
        });
      });
};

const getGenres = async (dispatch, genres) => {
  if (genres.length === 1)
    await api
      .get(
        `https://api.themoviedb.org/3/genre/movie/list?api_key=${api_key}&language=pt-BR`
      )
      .then(response => {
        dispatch({
          type: LOAD_GENRES,
          payload: response.data.genres
        });
      });
};
const filterMovies = (movies, pages) => {
  if (pages) {
    filteredMovies = movies.slice(0, 5);
  }
};
const Panel = ({ movies, genres, page, dispatch }) => {
  useEffect(() => {
    getGenres(dispatch, genres);
    discover(dispatch, movies);
    filterMovies(movies, page);
  }, [dispatch, genres, movies, page]);
  /*  useEffect(() =>{
  }, [page]);
 */
  return (
    <Container>
      {filteredMovies.map(element => (
        <Card key={element.id}>
          <ImageField>
            <ImageMovie path={element.poster_path} />
          </ImageField>
          <CardContent>
            <Title>{element.title}</Title>
            <Rated>
              <Arc>{`${element.vote_average * 10}%`}</Arc>
            </Rated>
            <Content>
              <Description>
                {element.overview || "Sem Descrição para este Filme"}
              </Description>
              <ChipsField>
                {element.genre_ids.map(id => (
                  <Chip key={id}>
                    {genres.find(genre => genre.id === id).name}
                  </Chip>
                ))}
              </ChipsField>
            </Content>
          </CardContent>
        </Card>
      ))}
    </Container>
  );
};
const mapStateToProps = state => ({
  ...state,
  movies: state.films.movies,
  genres: state.films.genres,
  page: state.films.page
});

export default connect(mapStateToProps)(Panel);
