import React from "react";

import { Container } from "./styles";

const Header = () => <Container>Movies</Container>;

export default Header;
