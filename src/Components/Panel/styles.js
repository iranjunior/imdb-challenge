import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
  background: #fff;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
`;
export const Card = styled.div`
  background: #ccc;
  width: 75%;
  display: flex;
  min-height: 300px;
  display: flex;
  flex-wrap: nowrap;
  flex-direction: row;
  margin: 50px auto;
  :hover {
    box-shadow: 5px 5px 10px 1px #116193;
    transition: all 300ms ease-in-out;
  }
`;
export const ImageField = styled.div`
  width: 25%;
  height: 300px;
`;
export const ImageMovie = styled.img.attrs(props => ({
  src: `https://image.tmdb.org/t/p/w500${props.path}`
}))`
  height: 300px;
`;
export const Title = styled.div`
  width: 100%;
  height: 50px;
  padding-left: 100px;
  background: #116193;
  color: #00e8e4;
  display: flex;
  align-items: flex-end;
  font-size: 1.8em;
`;

export const CardContent = styled.div`
  width: 100%;
  overflow: hidden;
`;

export const Content = styled.div`
  margin-left: 20px;
`;
export const Rated = styled.div`
  height: 60px;
  width: 60px;
  background: #116193;
  border-radius: 50%;
  margin-left: 20px;
  margin-top: -30px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const Arc = styled.span`
  background: #116193;
  height: 50px;
  width: 50px;
  border-radius: 50%;
  color: #00e8e4;
  border: 2px solid #00e8e4;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.2em;
`;
export const Description = styled.p`
  margin: 10px;
  text-align: justify;
`;
export const ChipsField = styled.div`
  margin-top: 20px;
  display: flex;
`;
export const Chip = styled.div`
  line-height: 1.5;
  background: #fff;
  margin: 0;
  margin-left: 10px;
  padding: 1px 20px;
  border: 2px solid #116193;
  border-radius: 32px 32px 32px;
  font-size: 1em;
  color: #116193;
`;
