import React from "react";
import Header from "../../Components/Header";
import Panel from "../../Components/Panel";
import Pagination from "../../Components/Pagination";

import { Container } from "./styles";

const Home = () => (
  <Container>
    <Header />
    <Panel />
    <Pagination />
  </Container>
);

export default Home;
