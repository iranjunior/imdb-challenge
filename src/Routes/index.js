import React from "react";
import Home from "../Pages/Home";
import { Switch, Route } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import history from "./history";

const Routes = () => (
  <ConnectedRouter history={history}>
    <Switch>
      <Route path="/" component={Home} />
    </Switch>
  </ConnectedRouter>
);

export default Routes;
