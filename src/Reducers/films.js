import {
  LOAD_MOVIES,
  LOAD_GENRES,
  CHANGE_PAGE
} from "../Constants/actionsType";

export default (
  state = { page: 1, movies: [], genres: [{ id: 0, name: "" }] },
  action
) => {
  switch (action.type) {
    case LOAD_MOVIES:
      return { ...state, movies: action.payload };
    case LOAD_GENRES:
      return { ...state, genres: action.payload };
    case CHANGE_PAGE:
      return { ...state, page: action.payload };
    default:
      return state;
  }
};
