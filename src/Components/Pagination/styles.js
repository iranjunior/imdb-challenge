import styled, { css } from "styled-components";

export const Container = styled.div`
  margin: 20px 0;
`;

export const PagesField = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  letter-spacing: 25px;
  color: #116193;
`;

export const Page = styled.a`
  text-decoration: none;
  ${props =>
    props.active &&
    css`
      height: 40px;
      width: 40px;
      background: #116193;
      border-radius: 50%;
      display: flex;
      justify-content: center;
      align-items: center;
      color: #00e8e4;
      letter-spacing: 0;
      margin: 10px;
      font-size: 1.3em;
    `}
  cursor: pointer;
  :hover {
    font-size: 1.5em;
    font-weight: bold;
  }
`;
export const Arc = styled.span`
  background: #116193;
  height: 30px;
  width: 30px;
  border-radius: 50%;
  color: #00e8e4;
  border: 2px solid #00e8e4;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.2em;
`;
