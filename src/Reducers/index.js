import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import films from "./films";
import pagination from "./pagination";

import history from "../Routes/history";

export default combineReducers({
  films,
  pagination,
  router: connectRouter(history)
});
