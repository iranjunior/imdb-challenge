export const TEST = "TEST";
export const REQUEST_TOKEN = "REQUEST_TOKEN";
export const SESSION_ID = "SESSION_ID";
export const LOAD_MOVIES = "LOAD_MOVIES";
export const LOAD_GENRES = "LOAD_GENRES";
export const CHANGE_PAGE = "CHANGE_PAGE";
export const PAGES_TOTALS = "PAGES_TOTALS";
