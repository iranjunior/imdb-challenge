import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
      margin: 0;
      padding: 0;
      font-family: "Abel";
      display: flex;
      flex-direction: column;
      justify-content: center;
      flex-wrap: wrap;
  }
`;
