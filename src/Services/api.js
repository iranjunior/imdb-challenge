import axios from "axios";
import { api_key } from "../Config/vars";

const api = axios.create({
  baseURL: "https://api.themoviedb.org/3/",
  timeout: 100000
});

export const getGenres = async () => {
  await api
    .get(
      `https://api.themoviedb.org/3/genre/movie/list?api_key=${api_key}&language=pt-BR`
    )
    .then(response => response.data);
};

export default api;
