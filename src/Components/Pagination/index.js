import React from "react";
import { connect } from "react-redux";
import { Container, PagesField, Page, Arc } from "./styles";
const handlePage = e => {
  console.log(e.target.innerHTML);
};

const Pagination = (pageCurrent, pageTotal) => {
  return (
    <Container>
      <PagesField>
        <Page active>
          <Arc>1</Arc>
        </Page>
        <Page onClick={e => handlePage(e)}>2</Page>
        <Page onClick={e => handlePage(e)}>3</Page>
        <Page onClick={e => handlePage(e)}>4</Page>
        <Page onClick={e => handlePage(e)}>5</Page>
      </PagesField>
    </Container>
  );
};
const mapStateToProps = state => ({
  ...state,
  pageCurrent: state.pagination.page,
  pageTotal: state.pagination.pages
});
export default connect(mapStateToProps)(Pagination);
