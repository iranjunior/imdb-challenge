/* eslint-disable no-undef */
const dotenv = require("dotenv");
const path = require("path");

dotenv.config({
  path: path.resolve(__dirname, "../../.env"),
  sample: path.resolve(__dirname, "../../.env.example")
});

module.exports = {
  api_key: process.env.REACT_APP_API_KEY,
  username: process.env.REACT_APP_USERNAME,
  password: process.env.REACT_APP_PASSWORD
};
