import styled from "styled-components";

export const Container = styled.header`
  position: fixed;
  width: 100%;
  height: 40px;
  background: #116193;
  color: #00e8e4;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 2em;
`;
