import { CHANGE_PAGE, PAGES_TOTALS } from "../Constants/actionsType";

export default (state = { page: 1 }, action) => {
  switch (action.type) {
    case CHANGE_PAGE:
      return { ...state, page: action.payload };
    case PAGES_TOTALS:
      return { ...state, pages: action.payload };
    default:
      return state;
  }
};
